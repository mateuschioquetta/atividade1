package br.com.proway.Ativiade1.enums;

import lombok.Getter;

@Getter
public enum Enum {
    POLICIALMILITAR(1L, "Policial Militar"),
    POLICIALCIVIL(2L, "Policial Civil"),
    POLICIALFEDERAL(3L, "Policial Federal"),
    POLICIACANINA(4L, "Policial K9");
    
    private final Long id;
    private final String descricao;
    
    Enum(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }
    
}
