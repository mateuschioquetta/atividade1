package br.com.proway.Ativiade1.controllers;

import br.com.proway.Ativiade1.utils.BancoDados;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/policial")
public class PolicialController {
    BancoDados bancoDados = new BancoDados();

    @GetMapping
    public ResponseEntity<String> get() {
        return ResponseEntity.ok(bancoDados.selecionarPolicial().toString());
    }

}
