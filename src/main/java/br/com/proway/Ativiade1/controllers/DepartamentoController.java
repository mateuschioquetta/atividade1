package br.com.proway.Ativiade1.controllers;

import br.com.proway.Ativiade1.models.Departamento;
import br.com.proway.Ativiade1.utils.BancoDados;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("/departamento")
public class DepartamentoController {
    BancoDados bancodados = new BancoDados();

    @GetMapping("")
    public ResponseEntity<ArrayList<Departamento>> getAll() {
        return ResponseEntity.ok(bancodados.selecionarDepartamento());
    }
}
