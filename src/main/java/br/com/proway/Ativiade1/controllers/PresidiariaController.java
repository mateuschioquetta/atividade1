package br.com.proway.Ativiade1.controllers;

import br.com.proway.Ativiade1.models.Policial;
import br.com.proway.Ativiade1.utils.BancoDados;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/presidiario")
public class PresidiariaController {
    BancoDados bancoDados = new BancoDados();

    @GetMapping
    public ResponseEntity<String> get() {
        return ResponseEntity.ok(bancoDados.selecionarPresidiario().toString());
    }
}
