package br.com.proway.Ativiade1;

import br.com.proway.Ativiade1.enums.Enum;
import br.com.proway.Ativiade1.models.Departamento;
import br.com.proway.Ativiade1.models.Policial;
import br.com.proway.Ativiade1.models.Presidiario;
import br.com.proway.Ativiade1.utils.BancoDados;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ativiade1Application {
	public static void main(String[] args) {
		Departamento departamento01 = new Departamento();
		Departamento departamento02 = new Departamento();
		Departamento departamento03 = new Departamento();
		Policial policial06 = new Policial();
		Presidiario elemento = new Presidiario();

		departamento01.setId(1L);
		departamento01.setNome("Departamento Policial Muito Legal");
		departamento01.setDistrito(10);

		departamento02.setId(2L);
		departamento02.setNome("Departamento Policial 99");
		departamento02.setDistrito(99);

		departamento03.setId(3L);
		departamento03.setNome("Departamento loucademia de policia");
		departamento03.setDistrito(69);

		policial06.setId(2L);
		policial06.setNome("Jake Peralta");
		policial06.setCargo("Investigação");
		policial06.setSetor(Enum.POLICIALCIVIL);

		elemento.setId(24L);
		elemento.setNome("Wilson");
		elemento.setVulgo("Wilssin");
		elemento.setCrimePresidiario("Homicídio culposo de seu companheiro em um assalto à joalheiria");

		BancoDados.inserirDepartamento(departamento01);
		BancoDados.inserirDepartamento(departamento02);
		BancoDados.inserirPolicial(policial06);
		BancoDados.inserirPresidiario(elemento);

		SpringApplication.run(Ativiade1Application.class, args);
	}

}
