package br.com.proway.Ativiade1.utils;

import br.com.proway.Ativiade1.models.Departamento;
import br.com.proway.Ativiade1.models.Policial;
import br.com.proway.Ativiade1.models.Presidiario;
import lombok.Data;

import java.util.ArrayList;

@Data
public class BancoDados {
    private static ArrayList<Policial> listaPoliciais = new ArrayList<>();
    private static ArrayList<Presidiario> listaPresidiarios = new ArrayList<>();
    private static ArrayList<Departamento> DepartamentoPolicial = new ArrayList<>();

    public static void inserirDepartamento(Departamento departamento) {
        DepartamentoPolicial.add(departamento);
    }

    public static void inserirPolicial(Policial policial) {
        listaPoliciais.add(policial);
    }

    public static void inserirPresidiario(Presidiario presidiario) {
        listaPresidiarios.add(presidiario);
    }

    public ArrayList<Departamento> selecionarDepartamento() {
        return DepartamentoPolicial;
    }

    public ArrayList<Presidiario> selecionarPresidiario() {
        return listaPresidiarios;
    }

    public ArrayList<Policial> selecionarPolicial() {
        return listaPoliciais;
    }
}
