package br.com.proway.Ativiade1.models;

import lombok.Data;

import java.util.ArrayList;

@Data
public class Presidiario {
    private Long id;
    private String nome;
    private String vulgo;
    private String crimePresidiario;
}
