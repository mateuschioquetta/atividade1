package br.com.proway.Ativiade1.models;

import lombok.Data;

@Data
public class Departamento {
    private Long id;
    private String nome;
    private Integer distrito;


}
